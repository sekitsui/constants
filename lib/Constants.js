'use strict';

exports.SEKITSUI_OPCODES = {
	DISPATCH: 0, // DISPATCH (server<->client)
	META: 1, // META (server<-client)
	QUERY: 2, // QUERY (server<->server, server<-client)
	QUERY_WAIT: 3, // QUERY_WAIT (server<->server, server->client)
	QUERY_RESPONSE: 4, // QUERY_RESPONSE (server<->server, server<->client)
	QUERY_RAW: 5, // QUERY_RAW (server<-client)
	HELLO: 10, // HELLO (server->client)
	IDENTIFY: 11, // IDENTIFY (server<-client)
	READY: 12, // READY (server->client)
	HEARTBEAT: 19, // HEARTBEAT (server<->client)
	HEARTBEAT_ACK: 20, // HEARTBEACT_ACK (server<->client)
};

exports.SEKITSUI_CLOSE_CODES = {
	UNKNOWN: 4000, // No idea whats wrong, client should try to reconnect
	DECODE_ERROR: 4001, // Client didn't send JSON / missing 'op' and/or 'd' in packet
	INVALID_OPCODE: 4002, // Client sent unknown opcode
	NOT_IDENTIFIED: 4003, // Client trying to send other opcodes prior to identifing
	IDENTIFY_TIMEOUT: 4004, // Client failed to send Identify payload in time
	ALREADY_IDENTIFIED: 4005, // Client tried to identify more then once
	SESSION_TIMEOUT: 4006, // Client Session timed out. Possibly caused by not enough hearbeats
	INVALID_STATE: 4007, // Client and server appear to be out sync, sending improper payloads out of order
};

exports.SEKITSUI_ERROR_CODES = {
	UNKNOWN: 4100, // No idea whats wrong, client should retry.
	QUERYLESS_QUERY: 4101, // Attempted to run query opcode without a 'q'uery
};

exports.API_OPCODES = {
	DISPATCH: 0,
	HELLO: 10,
	HEARTBEAT: 11,
	HEARTBEAT_ACK: 12,
};
